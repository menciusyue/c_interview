/*
值传递、指针传递、引用传递 
*/
#include<iostream>
using namespace std;

void Exchange(int a,int b){
	int tmp;
	tmp = a;
	a = b;
	b = tmp;
} 
void ExchangeP(int *pa,int *pb){
	int tmp;
	tmp = *pa;
	*pa = *pb;		//地址不能乱改不能写成pa = pb； 
	*pb = tmp;
	
}
void Exchange_(int &pa,int &pb){
	int tmp;
	tmp = pa;
	pa = pb;
	pb = tmp;
	 
}
int main(){
	int a = 3,b = 5;
	Exchange(a,b);
	cout<<"传值"<<endl;
	cout<<a<<" "<<b<<endl;
	ExchangeP(&a,&b);
	cout<<"传指针"<<endl;
	cout<<a<<" "<<b<<endl;
	Exchange_(a,b);
	cout<<"传指针"<<endl;
	cout<<a<<" "<<b<<endl;
	
	
} 
