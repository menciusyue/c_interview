#include<iostream>

using namespace std;

class A{
private:
	int x;
public:
	A(int xp):x(xp){\
		cout<<"A的构造被执行"<<endl;
	}
	~A(){
		cout<<"A析构"<<endl;
	}
};

class B{
public:
	B(){
		cout<<"B的构造被执行"<<endl;
	}
	~B(){
		cout<<"B析构"<<endl; 
	}
};

class C:public A{
private:
	int y;
	B b;
public:
	C(int xp,int yp):A(xp),b(){
		y = yp;
		cout<<"C的构造被执行"<<endl;
	}
	~C(){
		cout<<"C析构"<<endl;
	}
};
int main(){
	C c(1,3);		//调用完之后自动调用析构函数 
	//c.~C();
	//cout<<c.A(4)<<endl;
}