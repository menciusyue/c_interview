#include<iostream>
using namespace std;

class A{
public:
	virtual void fun1(){
		cout<<"A::fun1"<<endl;
	}
	virtual void fun2(){
		cout<<"A::fun2"<<endl;
	}
};
class B:public A{
public:
	virtual void fun1(){
		cout<<"B::fun1"<<endl;
	}
	virtual void fun2(){
		cout<<"B::fun2"<<endl;
	}
};
int main (){
	A* a = new B;			//指向a的指针由B实例化来，因此执行的是B::fun1() 
	a->fun1();
	a->A::fun2();			//指明了虚函数的位置 A::fun2()
}