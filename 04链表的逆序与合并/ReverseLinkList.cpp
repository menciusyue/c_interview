#include<iostream>
using namespace std;
struct Node{
    int data;
    Node* next;
};
typedef struct Node Node;
Node* ReverseLinkList(Node* head){
    Node* p1 = head;
    Node* p2 = p1->next;
    Node* p3 = p2->next;
    p1->next = NULL;
    if(p3->next != NULL){
        p2->next = p1;
        p1 = p2;
        p2 = p3;
        p3 = p3->next;
    }
    p2->next = p1;
    head =p2;
    return p2;
}
int main(){
    
}
