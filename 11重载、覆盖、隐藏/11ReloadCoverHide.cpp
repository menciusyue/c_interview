/*
重载、覆盖、隐藏 
*/ 
#include<iostream>
using namespace std;
/*
重载：同一类定义中的函数名相同的成员函数才存在重载关系，主要特点是参数类型或者参数数目的不同 
重载与虚函数无关 
重载的特征：
1. 同一个类中函数名相同的成员函数
2. 参数不同（类型或数目）
3. virtual不影响函数重载 
*/ 
class A{
private:
	int x;
public:
	void fun(int);			//重载关系的三个函数， 
	void fun(double,double);
	virtual fun();
};

/*
覆盖：派生类的成员函数覆盖了基类中的同名成员函数
要求：
1. 两个函数的参数和类型都相同
2. 基类中的函数必须为虚函数

特征：
1. 分别位于派生类和基类的同名函数 
2. 参数类型与参数数目相同
3. 基类中必须为虚函数 
 
*/
class L{				//基类 
private:
	int x;
public:
	virtual void fun1(){cout<<"L:fun1"<<endl;}; 
	void fun1(int){cout<<"L:fun1i"<<endl;}; 
	void fun2(){cout<<"L:fun2"<<endl;};
}; 
class M:public L{
public:
	void fun1(){cout<<"M:fun1"<<endl;};		//M的fun1覆盖了L的fun1 
	void fun2(){cout<<"M:fun2"<<endl;};
	void fun1(int){cout<<"M:fun1i"<<endl;};
};

/*
隐藏：某些情况下派生类的函数屏蔽了基类函数中的同名函数
特征：
1. 派生类的函数参数与基类同名函数相同，且基类同名函数不虚，基类同名函数被隐藏。 
2. 派生类的函数参数与基类同名函数不同，基类同名函数一定被隐藏。 
*/
class X{
public:
	virtual x1(int){cout<<"X:x1"<<endl;};
	x2(int){cout<<"X:x2"<<endl;};
	//x2(){cout<<"X:x2"<<endl;};			同名函数都被隐藏 
};
class Y:public X{
public: 
	x1(){cout<<"Y:x1"<<endl;};		//参数不同，虚不虚都是隐藏 
	x2(int){cout<<"Y:x2"<<endl;};	//参数相同且不虚，   隐藏 
};
int main(){
	//M m,n;
	//m.fun1(23);
	//n.fun2();
	Y y;
	y.x1(); 		//x1（int）被隐藏 
	//y.x2();		//非法 
} 