#include<iostream>
#include<string.h>

using namespace std;

class String{
public:
	String(const char* str);	//构造 
	String(const String& Str);	//复制构造
	~String();					//析构
	String & operator = (const String& s);	//运算符重载 
	StringPrint();			//打印 
	char* m_data; 
};
 
//构造 
String::String(const char* str){
	if(strlen(str) ==0){
		m_data = new char[1];
		m_data[0]='\0';
	}
	else{
		char* m_data = new char[strlen(str)+1];
		strcpy(m_data,str);
	}
}
//复制构造
String::String(const String& Str){
	m_data = new char[strlen(Str.m_data)+1];
	strcpy(this->m_data,Str.m_data);
} 
//赋值
String& String::operator = (const String& s){
	if(this->m_data == s.m_data){
		return *this;
	}
	else{
		delete []m_data;		//删除原先内存 
		m_data = new char[strlen(s.m_data)+1];		//新开 
		strcpy(m_data,s.m_data);
		return *this;
	}
} 
String::~String(){
	delete []m_data; 
	m_data = NULL;
} 
//print
String::StringPrint(){
	cout<<this->m_data<<endl;
}
int main (){
	
	const char* c = "cfda";
	String S(c);
	S.StringPrint();    
	

}