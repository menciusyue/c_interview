# String类的具体实现

string的内容其实就是C中的字符串，在C中是char*型，在C++中是string类型。

```C++
//C
char *str = "mengziyue";
//C++
string str = "mengziyue";
string str("mengziyue");
```

String类的简单实现

```C++
#include<iostream>
#include<string.h>

using namespace std;


class String{				//String的定义
public:
    String(const char *str=NULL);		//构造
    String(const String& Str);		//复制构造
    String& operator = (const String& S);		//运算符重载
    PrintString();
    ~String();
    char *m_data;
};

String::String(const char *str){		//判断初始化构造函数的字符串是否为空
    if(str == NULL){					
        m_data = new char[1];
        m_data[0] = '\0';
    }
    else{
        m_data = new char[strlen(str)+1];
        strcpy(this->m_data,str);
    }
}

String::String(const String& Str){		//复制构造函数，无需判断
    m_data = new char[strlen(Str.m_data)+1];
    strcpy(this->m_data,Str.m_data);
}

String& String::operator = (const String& S){	//运算符重载，先判断两串是否相等
    if(S.m_data == this->m_data){				//若相等则直接返回this指针
        return *this;
    }
    else{										//若不等则需要先删除原先开辟的内存，再重新分配
        delete []m_data;						//这是为了防止被赋值的串的大小与赋值串的大小不同
        m_data = new char[strlen(S.m_data)+1];	//而导致拷贝串时发生错误
        strcpy(this->m_data,S.m_data);
    }
}
String::~String(){								//释放内存时需要将原指针指向空，避免成为野指针
    delete []m_data;
    m_data = NULL;
}
String::PrintString(){
    cout<<this->m_data<<endl;
}
int main(){
    String S("dafagag");
    S.PrintString();
    String S2;
    S2 = S;
    S2.PrintString();
    return 0;
}
```











参考链接：

1. [C++中string的实现原理](https://www.cnblogs.com/downey-blog/p/10470912.html)