#include<iostream>
#include<string.h> 

using namespace std;


class String{
public:
	String(const char* str=NULL);		//构造,默认空串 
	String(const String& Str);		//复制构造
	String & operator = (const String& S); 	//运算符重载 
	String & operator + ( String& S1); 		//未能正常运行 
	~String();		//析构 
	StringPrint();
	char* m_data;
}; 
//构造 
String::String(const char* str){
	if(str == NULL){
		m_data = new char[1];
		m_data[0] == '\0';
	}
	else{
		m_data = new char[strlen(str)+1];
		strcpy(m_data,str);
	}
}
//复制构造函数
String::String(const String& Str){
	m_data = new char[strlen(Str.m_data) + 1];
	strcpy(this->m_data,Str.m_data);
} 
//赋值函数
String& String::operator =(const String& S) {
	if(this->m_data == S.m_data){
		return *this;
	}
	else{
		delete []m_data;
		m_data = new char [strlen(S.m_data)+1];
		strcpy(this->m_data,S.m_data);
	}
}
//析构函数
String::~String(){
	delete []m_data;
	m_data = NULL;
} 
//pirnt
String::StringPrint(){
	cout<<this->m_data<<endl;
}
//+
String& String::operator + ( String& S1){
	if(S1.m_data == NULL){
		return *this;
	}
	else if(this->m_data == NULL){
		return S1;
	}
	else if(this->m_data == NULL && S1.m_data == NULL){
		cout<<"error"<<endl;
	}
	else{
		char *tmp = new char[strlen(this->m_data) + strlen(S1.m_data) +1];
		memcpy(tmp,this->m_data,strlen(this->m_data));
		strncat(tmp,S1.m_data,strlen(S1.m_data));
		this->m_data = tmp;
		delete []tmp;
		tmp = NULL;
		
	}
}
int main(){
	//String str1 = "mengziyue";

	const char* c="MMMMMZY";

	String str2("menciusyue");		//调用构造函数进行初始化 
	str2.StringPrint();
	String str,str4(c);
	cout<<strlen(str.m_data)<<endl;
	str4.StringPrint();
	str = str2;
	str.StringPrint();
	cout<<strlen(str.m_data)<<"  "<<strlen(str2.m_data)<<endl;
	str.StringPrint();
	//操作符重载
	//str2 + str4;			//功能不全 
	return 0;
}