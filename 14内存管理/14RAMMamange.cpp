/*
内存管理方式：
1. 自动存储
2. 静态存储
3. 动态存储

1.自动存储： 对于函数形参、函数内部变量、 结构体变量等变量 ，编译器在执行过程中会自动在栈内为他们分配空间
并在函数结束后自动销毁，所以它们的生命周期只存在于当前代码块。 


2. 静态存储（编译器预存储）（永久存储） 
每个程序对应着一个静态存储区（全局数据区），编译器会对某些程序实体预分配存储地址和内存空间，程序一开始执行
这些变量就会被创建，直到整个程序结束才会被释放。
	1.extern关键字用于声明全局变量，该变量在函数和类的外部定义，也叫外部变量，外部变量的声明有两种方式
		1.1 定义性声明：extern 类型 变量名 = 初始化值；
					eg: extern doubel GlobalDouble = 32.3; 
					定义性声明时必须进行初始化
		1.2 引用性声明： 
	 					extern int  GlobalInt;	//必须是已声明的外部变量。 
 
 
*/
#include<iostream>
using namespace std;
int num1;						//全局变量要在定义时初始化，若未初始化编译器会将其自动初始化为0 
extern double GlobalDouble = 325.4;
int add(int a,int b){
	if(a > 0 && b > 0){
		int result = a +b;			//result的生命周期只存在于if语句内 
	}
	//return result;		 		此时result已经被销毁，无法再使用 
}
	
int main (){
	extern int num1; 		//引用性声明，必须在外面已经声明了 
	cout<<GlobalDouble<<endl;
	cout<<num1<<endl;
}
