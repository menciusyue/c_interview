# 类模板的使用

注意在每次类模板函数时都需要声明一个类模板

```C++
#include<iostream>
using namespace std;

template <class T,int num>
class Stack{
private:
	T t[num];
	int point;
public:
	 Stack();
	 bool IsEmpty();
	 bool IsFull();
	 bool Push(const T& a);
	 bool Pop(T& result);
};
template <class T,int num>
Stack<T,num>::Stack(){
	point = 0;		//指向栈底 
}
template <class T,int num>
bool Stack<T,num>::IsEmpty(){
	return point == 0;
}
template <class T,int num>
bool Stack<T,num>::IsFull(){
	return point == num;
}
template <class T,int num>
bool Stack<T,num>::Pop(T& result){			//出栈先判断栈是否为空 Pop还需要一个元素来存储弹出的结果 
	if(IsEmpty()){
		return 0;
	}
	else{
		point--;
		result = t[point];
	}
	return true;
}
template <class T,int num>
bool Stack<T,num>::Push(const T& a){		//入栈先判断栈是否已满 
	if(IsFull()){
		return 0;
	}
	else{
		
		t[point] = a;		//注意先后顺序，point指的实际上是栈中最顶部元素上面的位置 
		point++;
	}
	return true;
}
int main(){
	Stack<int,10> S;		//注意初始化的方式Stack<int,10> 
	cout<<S.IsEmpty()<<endl;
	for(int i = 0;i < 10;i++){
		S.Push(i);
	}
	int res;		//该变量用来存储弹出的结点 
	while(S.Pop(res)){
		cout<<res<<"  ";
	}
	cout<<endl;
}
```

