#include<iostream>
using namespace std;

template <class Ex>			
Ex Max(Ex a,Ex b){			//隐式实例化 
	return a>b?a:b;
}
template int Max<int>(int,int);	//显式实例化 
int Max(int a,int b){		
	cout<<"Obverious"<<endl;
	return a>b?a:b;
}
int main(){
	int ia = 32,ib = 3;
	double da = 32.5,db = 8434.3;;
	cout<<Max(ia,ib)<<endl;		//掉用的是显式实例化的函数Max(int,int)
	cout<<Max(da,db)<<endl;
	return 0;
} 
