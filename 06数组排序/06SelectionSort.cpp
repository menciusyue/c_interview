/*
选择排序
跟冒泡思想一样，只不过每次记录下最小的元素加到已排好序的数组后面
居然依次就过了。。。。出乎意料
*/

#include <iostream>
using namespace std;

void swap(int& a,int& b){
	int t = a;
	a = b;
	b = t;
}
void SelectSort(int* arr,int len){
	for(int i = 0;i < len;i++){
		int tmpMinIndex = i;
		for(int j = i+1;j < len;j++){
			if(arr[tmpMinIndex] > arr[j]){
				tmpMinIndex = j;
			}
		}
		swap(arr[tmpMinIndex],arr[i]);
	}
}
void print(int* a,int len){
	for(int i = 0;i < len;i++){
		cout<<a[i]<<" ";
	}
	cout<<endl;
}
int main(int argc, char const *argv[])
{
	int a[10] = {3,4,5,1,6,8,9,0,5,4};
	SelectSort(a,10);
	print(a,10);
	return 0;
}
