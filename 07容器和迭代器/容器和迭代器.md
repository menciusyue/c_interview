# 容器和迭代器

1. vector、list、deque

   ```c++
   #include<iostream>
   #include<vector>
   #include<deque>
   #include<list>
   using namespace std;
   int main(){
       vector<int> obV(2,4);
       vector<int>::iterator iterV;
       for(iterV = obV.begin();iterV != obV.end();iterV++){
       	cout<<*iterV<<" ";
   	}
   	cout<<endl;
   	for(int i = 0;i < obV.size();i++){
   		cout<<obV[i]<<" ";
   	}
   	cout<<endl;
       cout<<obV.size()<<endl;
       
       deque<int> obD(4,45);
       for(int i = 0;i < obD.size();i++){
   		cout<<obD[i]<<" ";
   	}
   	cout<<endl;
   	
   	list<int> obL(33,4);
   	/*					list地址是不连续的， 因此不能用下标输出 
   	for(int i = 0;i < obL.size();i++){
   		cout<<obL[i]<<" ";
   	}
   	cout<<endl;
   	*/
   	list<int>::iterator iterL;
       for(iterL = obL.begin();iterL != obL.end();iterL++){
       	cout<<*iterL<<" ";
   	}
   	cout<<endl;
   	
   }
   ```

   