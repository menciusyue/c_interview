/* 动态内存分配 ：new；delete。malloc；free。 
1。 new与delete的用法
类型名 *指针类型 = new 类型名；    					//基本类型
类型名 *指针类型 = new 类型名【数组大小】；			//数组类型 
int *p = new int; 
*/
#include<iostream>
#include<stdlib.h>
using namespace std;
int main (){
	/*
	int *p = new int;
	cin>>*p;
	cout<<**&*&*&*&*&*&*&*&*&p<<endl;
	int arrSize = 0; //数组大小，默认为0；
	cin>>arrSize;		//输入数组大小
	//开辟内存空间
	int* pArr = new int[arrSize];
	//赋值
	for(int i = 0; i < arrSize; i++){
		cin>>pArr[i];
	} 
	//打印
	for(int i = 0; i < arrSize; i++){
		//cout<<pArr[i];
		printf("%d\t",pArr[i]);
	} 
	delete []pArr;
	pArr = NULL; 
	*/
	//malloc;free
	int arrSize = 0;
	cin>>arrSize;
	int *p = (int*)malloc(sizeof(int)*arrSize);
	for(int i = 0; i < arrSize; i++){
		cin>>*(p+i);
	}
	for(int i = 0; i < arrSize; i++){
		//cout<<pArr[i];
		printf("%d\t",p[i]);
	}
	free(p);
	//delete []p; 			//用delete也行 
	int *np = (int*)malloc(sizeof(int));
	cin>>*np;
	cout<<*np;
	free(np);
	return 0;
} 